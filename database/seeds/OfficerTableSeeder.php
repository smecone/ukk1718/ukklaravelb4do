<?php

use Illuminate\Database\Seeder;

class OfficerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Officer::create([
            'name' => 'Officer',
            'username' => 'firsto',
            'password' => Hash::make('free4you'),
            'api_token' => str_random(80),
        ]);
    }
}
