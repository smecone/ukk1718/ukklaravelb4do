@extends('main')

@section('title', 'Officer - Form Book Return')
@section('sidenav', 'true')
@section('content')
    <div class="container">
        <div class="card-panel white">  
            <h4>Form Book Return</h4>
            <form id="book-lending" action="" method="POST">
                {{ method_field('POST') }}
                {{ csrf_field() }}
                <input type="hidden" id="officer_id" class="validate" name="officer_id" value="{{ Auth::user()->id }}">

                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" id="loan_id" class="validate" name="loan_id">
                        <label for="loan_id">Loan ID</label>
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="book_id" class="validate" placeholder="Book ID" name="book_id">                        
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="book_isbn" class="validate" placeholder="Book ISBN" disabled>                
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="book_title" class="validate" placeholder="Book Title" disabled>                    
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="member_id" class="validate" placeholder="Member ID" name="member_id">
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="member_nis" class="validate" placeholder="Member NIS" disabled>                
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="member_name" class="validate" placeholder="Member Name" disabled>                
                    </div>
        
                    <div class="col s12">  
                        <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>                
            </form>        
        </div>        
    </div>
@endsection
@push('scripts')
    <script>
        var form = $('#book-lending')
        form.find('#loan_id').change(function () { 
            getData("{{ url('api/officer/book/lending') }}/"+$(this).val(), 
                function (response) {
                    form.find('#book_id').val(response.data.book.id)
                    form.find('#member_id').val(response.data.member.id)
                    form.find('#officer_id').val(response.data.officer.id) 
                    form.find('#book_isbn').val(response.data.book.isbn)
                    form.find('#book_title').val(response.data.book.title) 
                    form.find('#member_nis').val(response.data.member.nis)
                    form.find('#member_name').val(response.data.member.name)                          
                }
            )
        })

        form.find('#book_id').change(function () {
            getData("{{ url('api/officer/book/') }}/"+$(this).val(), function (response) { 
                form.find('#book_isbn').val(response.data.isbn)
                form.find('#book_title').val(response.data.title)
            })
        })

        form.find('#member_id').change(function () {
            getData("{{ url('api/officer/member/') }}/"+$(this).val(), function (response) { 
                form.find('#member_nis').val(response.data.nis)
                form.find('#member_name').val(response.data.name)
            })
        })
        
        form.submit(function (event) { 
            event.preventDefault()
            postData("{{ route('api.officer.book.return') }}", $(this).serialize(), 
                function (response) { 
                    M.toast({
                        html: "Success",
                        outDuration: 1000,
                        classes: 'rounded',
                        completeCallback: function () { 
                            form.trigger('reset')
                        }
                    })
                }, 
                function (error) { 
                    M.toast({
                        html: "Error: "+error.statusText,
                        outDuration: 1000,
                        classes: 'rounded',
                        completeCallback: function () { 
                            console.log(error)
                        }
                    })
                }
            )
        })        
    </script>
@endpush