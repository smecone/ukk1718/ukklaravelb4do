<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
 * Officer Routes
 */
Route::prefix('/officer')->group(function () {
    Route::namespace('Auth\Officer')->group(function () {
        Route::get('/login', 'LoginController@showLoginForm')->name('officer.login')->middleware('guest');
        Route::post('/login', 'LoginController@login')->middleware('guest');
        Route::get('/logout', 'LoginController@logout')->middleware('auth:web-officer')->name('officer.logout');
    });

    Route::namespace('Officer')->group(function () {
        Route::middleware('auth:web-officer')->group(function () {
            Route::get('/list', 'OfficerController@list')->name('officer.list');

            Route::resource('/book', 'BookController', [
                'only' => [
                    'index', 'create'
                ],
                'names' => [
                    'index' => 'officer.book',
                    'create' => 'officer.book.create'
                ]
            ]);
    
            Route::resource('/book/lending', 'BookLendingController', [
                'only' => [
                    'index', 'create'
                ],
                'names' => [
                    'index' => 'officer.book.lending',
                    'create' => 'officer.book.lending.create'
                ]
            ]);
    
            Route::resource('/book/return', 'BookReturnController', [
                'only' => [
                    'index', 'create'
                ],
                'names' => [
                    'index' => 'officer.book.return',
                    'create' => 'officer.book.return.create'
                ]
            ]);

            Route::resource('/member', 'MemberController', [
                'only' => [
                    'index', 'edit'
                ],
                'names' => [
                    'index' => 'officer.member',
                    'edit' => 'officer.member.edit'
                ]
            ]);           
        });
    });    
});
Route::resource('/officer', 'Officer\OfficerController', [
    'only' => [
        'index', 'edit'
    ],
    'names' => [
        'index' => 'officer.home',
        'edit' => 'officer.edit'
    ]
])->middleware('auth:web-officer');

/**
 * Member Routes
 */
Route::prefix('/member')->group(function () {
    Route::namespace('Auth\Member')->group(function () {
        // Route::get('/login', 'LoginController@showLoginForm')->name('member.login');
        // Route::post('/login', 'LoginController@login');


        Route::middleware('auth:web-member')->group(function () {
            Route::get('/', function() {
                //return view('pages.member.home');
            })->name('member.home');
        });
    });
});
