<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookReturn extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "book_returns";

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * belongsTo BookLending
     */
    public function loan()
    {
        return $this->belongsTo('App\BookLending', 'loan_id');
    }

    /**
     * belongsTo Member
     */
    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }

    /**
     * belongsTo Officer
     */
    public function officer()
    {
        return $this->belongsTo('App\Officer', 'officer_id');
    }
}
