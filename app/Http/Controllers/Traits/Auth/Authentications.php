<?php

namespace App\Http\Controllers\Traits\Auth;

use Illuminate\Http\Request;

trait Authentications {
    /**
     * Reset the api_token field
     * 
     * @param mixed $user
     * @return bool
     */
    protected function resetApiToken($user) {        
        $user->api_token = str_random(80);
        return $user->save();
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        return response()->json([
            'message' => "Login Success, Welcome ".$user->{$this->username()}." :)",
            'redirectTo' => $this->redirectTo,
            'data' => $user,
        ]);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->resetApiToken($this->guard()->user());

        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }
}