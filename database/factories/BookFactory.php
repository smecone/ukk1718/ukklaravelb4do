<?php

use Faker\Factory as Faker;

$factory->define(App\Book::class, function () {
    $faker = Faker::create(config('app.locale'));

    return [
        'isbn' => $faker->isbn13,
        'title' => $faker->word,
        'description' => $faker->sentence,
        'categories' => $faker->word,
        'creator' => $faker->name,
        'publisher' => $faker->company,
        'stock' => $faker->randomNumber(2),
    ];
});
