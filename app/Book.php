<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "books";

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * hasMany to BookLending
     */
    public function loans()
    {
        return $this->hasMany('App\BookLending', 'book_id', 'id');
    }     
}
