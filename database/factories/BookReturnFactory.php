<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\BookReturn::class, function (Faker $faker) {
    // Get Book Lending and Update "Returned"
    $book_lending = App\BookLending::find(
        $faker->biasedNumberBetween(1, App\BookLending::count())
    );
    $book_lending->status = "returned";
    $book_lending->save();

    // Some variables
    $loan_id = $book_lending->id;
    $date_period = $book_lending->date_period;

    // Algorithm to make dynamic return    
    $date_return = Carbon::parse($date_period);
    if ($faker->biasedNumberBetween(0, 1) == 1) {
        $date_return->addDays($faker->randomDigit);
    }  
    $date_return->toDateTimeString();

    /**
     * Algorithm to count the fine
     * fine = out_period * 500/day
     */
    $fine = 0;
    if (($out_period = Carbon::parse($date_period)->diffInDays(Carbon::parse($date_return))) > 3) {
        $fine = $out_period * 500;
    }

    return [
        'fine' => $fine,
        'date_return' => $date_return,
        'loan_id' => $loan_id,
        'member_id' => function () use ($faker) {
            return App\Member::find(
                $faker->biasedNumberBetween(1, App\Member::count())
            );
        },
        'officer_id' => function () use ($faker) {
            return App\Officer::find(
                $faker->biasedNumberBetween(1, App\Officer::count())
            );
        },
    ];
});
