<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookLending extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "book_lendings";

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * hasOne BookReturn
     */
    public function return()
    {
        return $this->hasOne('App\BookReturn', 'loan_id', 'id');
    }

    /**
     * belongsTo Book
     */
    public function book()
    {
        return $this->belongsTo('App\Book', 'book_id');
    }

    /**
     * belongsTo Member
     */
    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }

    /**
     * belongsTo Officer
     */
    public function officer()
    {
        return $this->belongsTo('App\Officer', 'officer_id');
    }
}
