<?php

use Illuminate\Database\Seeder;

class MemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Member::create([
            'nis' => '8426',
            'name' => 'Smecone',
            'class' => 'RPL',
            'password' => Hash::make('free4you'),
            'api_token' => str_random(80)
        ]);
    }
}
