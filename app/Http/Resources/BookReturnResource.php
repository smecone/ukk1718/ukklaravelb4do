<?php

namespace App\Http\Resources;

use App\Http\Resources\BookLendingResource;
use App\Http\Resources\MemberResource;
use App\Http\Resources\OfficerResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BookReturnResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'fine' => $this->fine,
            'date_return' => $this->date_return,
            'loan' => $this->loan,
            'member' => $this->member,
            'officer' => $this->officer,
        ];
    }
}
