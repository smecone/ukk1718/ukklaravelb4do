@extends('main')

@section('title', 'Officer - Books Lending')
@section('sidenav', 'true')
@section('content')
    <div class="card-panel white">
        <div class="fixed-action-btn horizontal">
            <a href="{{ route('officer.book.lending.create') }}" class="btn-floating btn-large waves-effect waves-light red"><i class="large material-icons">add</i></a>
        </div>

        <!-- Table -->
        <table id="table-book-lending" class="display" width="100%"></table>
        
        <!-- Modal Structure -->
        <div id="modal-actions" class="modal bottom-sheet">
          <div class="modal-content">
            <h4>What you will do ? <span id="selected-id" class="badge teal-text"></span></h4>
            <ul id="actions" class="collection">
                <a href="#!" id="return" class="collection-item">Return</a>
                <a href="#!" id="update" class="collection-item">Update</a>
                <a href="#!" id="destroy" class="collection-item">Destroy</a>
            </ul>
          </div>
        </div>

        <!-- Modal Structure -->
        <div id="modal-return" class="modal">
            <div class="modal-content">
                <h4>Return Book</h4>
                <p>Are you sure ???</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class=" modal-action modal-close waves-effect waves-red btn-flat">Cancel</a>
                <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Yes</a>
            </div>
        </div>
        
        <!-- Modal Structure -->
        <div id="modal-update" class="modal">
            <div class="modal-content">
                <h4>Update Form</h4>
                <p>A bunch of text</p>
            </div>
            <div class="modal-footer">
                <a href="#!" id="modal-cancel" class=" modal-action modal-close waves-effect waves-red btn-flat">Cancel</a>
                <a href="#!" id="modal-submit" class=" modal-action modal-close waves-effect waves-green btn-flat">Submit</a>
            </div>
        </div>

        <!-- Modal Structure -->
        <div id="modal-destroy" class="modal">
            <div class="modal-content">
                <h4>Destroy Data</h4>
                <p>Are you sure want delete this data ?</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class=" modal-action modal-close waves-effect waves-red btn-flat">Cancel</a>
                <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Yes</a>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () { 
            getData("{{ route('api.officer.book.lending') }}", success)

            function return_book(data) {
                
            }

            function update(data) {
                
            }

            function destroy(data) {
                
            }

            function success(response) {
                var table = $('#table-book-lending').DataTable({
                    dom: 'Bfrtip',
                    data: response.data,
                    columns: [
                        {title: 'ID', data: 'id'},
                        {title: 'Date Loan', data: 'date_loan'},
                        {title: 'Date Period', data: 'date_period'},
                        {title: 'Status', data: 'status'},
                        {title: 'Book ID', data: 'book.id'},
                        {title: 'Book ISBN', data: 'book.isbn'},
                        {title: 'Book Title', data: 'book.title'},
                        {title: 'Member ID', data: 'member.id'},
                        {title: 'Member NIS', data: 'member.nis'},
                        {title: 'Member Name', data: 'member.name'},
                        {title: 'Officer ID', data: 'officer.id'},
                        {title: 'Officer Name', data: 'officer.name'}
                    ],
                    responsive: true,
                    buttons: ['pdf', 'excel']
                })

                table.on('click', 'tr', function () { 
                    var selected = table.row(this).data()
                    console.log(selected)

                    var modal_actions = $('#modal-actions')
                        modal_actions.modal('open')
                        modal_actions.find('#selected-id').text(
                            "Loan ID:"+selected.id+
                            " Book ID:"+selected.book.id+
                            " Member ID:"+selected.member.id
                        )

                    modal_actions.find('ul > a').click(function () { 
                        if($(this).is('#return')) {
                            return_book(selected)
                        }
                        if($(this).is('#update')) {
                            update(selected)
                        }
                        if($(this).is('#destroy')) {
                            destroy(selected)
                        }
                    })                      
                })
            }
        })
    </script>
@endpush