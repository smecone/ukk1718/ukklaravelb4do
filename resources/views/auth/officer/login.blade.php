@extends('main')

@section('title', 'Officer Login')

@section('content')
    <div class="container">
        <div class="card-panel white">
            <h4>Officer Login</h4>
            <form action="{{ route('officer.login') }}" id="form-login" action="POST">
                {{ method_field('POST') }}
                {{ csrf_field() }}
                <div class="input-field col s12 l6">
                    <input type="text" id="username" class="validate" name="username">
                    <label for="username">Username</label>
                </div>
                <div class="input-field col s12 l6">
                    <input type="password" id="password" class="validate" name="password">
                    <label for="password">Password</label>
                </div>  
                       
                <div class="row">
                    <div class="col s8 l2">   
                        <button class="btn waves-effect waves-light" type="submit" name="action">
                            Login
                        </button>
                    </div>
                    <div class="input-field col s8 12">
                        <label for="remember">
                            Remember Login
                            <input type="checkbox" id="remember" class="validate" name="remember">
                        </label>                        
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () { 
            $("#form-login").submit(function (event) { 
                event.preventDefault();
                $.ajax({
                    type: "POST",
                    url: $(this).action,
                    data: $(this).serialize()
                }).done(function (response) { 
                    console.log(response)
                    M.toast({
                        html: response.message,
                        outDuration: 1000,
                        classes: 'rounded',
                        completeCallback: function () { 
                            location.assign(response.redirectTo)
                        }
                    })
                }).fail(function (error) { 
                    console.log(error)
                    M.toast({
                        html: error.responseJSON.message,
                        outDuration: 2000,
                        classes: 'rounded',
                        completeCallback: function () { 
                            location.reload()
                        }
                    })
                })                
            })
        })
    </script>
@endpush