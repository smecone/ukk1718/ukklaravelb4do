@extends('main')

@section('title', 'Officer - Books')
@section('sidenav', 'true')
@section('content')
    <div class="card-panel white">
        <!-- Modal Trigger Insert -->
        <div class="fixed-action-btn horizontal">
            <a href="#modal-book" class="btn-floating btn-large waves-effect waves-light red modal-trigger"><i class="large material-icons">add</i></a>
        </div>

        <!-- Table -->
        <table id="table-book" class="display" width="100%"></table>
        
        <!-- Modal Structure -->
        <div id="modal-actions" class="modal bottom-sheet">
          <div class="modal-content">
            <h4>What you will do ? <span id="selected-id" class="badge teal-text"></span></h4>
            <ul id="actions" class="collection">
                <a href="#!" id="borrow" class="collection-item">Borrow</a>
                <a href="#!" id="update" class="collection-item">Update</a>
                <a href="#!" id="destroy" class="collection-item">Destroy</a>
            </ul>
          </div>
        </div>
        
        <!-- Modal Structure -->
        <div id="modal-book" class="modal">
            <form id="form-book" action="">
                <div class="modal-content">
                    <h4>Book Form</h4>                
                        {{ method_field('POST') }}
                        {{ csrf_field() }}
                    <div class="input-field col s12">
                        <input type="text" id="book-isbn" class="validate" name="isbn" placeholder="ISBN">
                        <!-- <label for="book-isbn">ISBN</label> -->
                    </div>              
                    <div class="input-field col s12">
                        <input type="text" id="book-title" class="validate" name="title" placeholder="Title">
                        <!-- <label for="book-title">Title</label> -->
                    </div>
                    <div class="input-field col s12">
                        <textarea id="book-description" class="materialize-textarea" name="description" placeholder="Description"></textarea>
                        <!-- <label for="book-description">Description</label> -->
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="book-categories" class="validate" name="categories" placeholder="Categories">
                        <!-- <label for="book-categories">Categories</label> -->
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="book-creator" class="validate" name="creator" placeholder="Creator">
                        <!-- <label for="book-creator">Creator</label> -->
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="book-publisher" class="validate" name="publisher" placeholder="Publisher">
                        <!-- <label for="book-publisher">Publisher</label> -->
                    </div>
                    <div class="input-field col s12">
                        <input type="number" id="book-stock" class="validate" name="stock" placeholder="Stock">
                        <!-- <label for="book-stock">Stock</label> -->
                    </div>                    
                </div>
                <div class="modal-footer">
                    <a href="#!" id="modal-cancel" class=" modal-action modal-close waves-effect waves-red btn-flat">Cancel</a>
                    <a href="#!" id="modal-update" class=" modal-action modal-close waves-effect waves-red btn-flat">Update</a>
                    <a href="#!" id="modal-submit" class=" modal-action modal-close waves-effect waves-green btn-flat">Submit</a>
                </div>
            </form>
        </div>        
        
        <!-- Modal Structure -->
        <div id="modal-borrow" class="modal">
            <form action="" method="post">
                {{ method_field('POST') }}
                {{ csrf_field() }}
                <input type="hidden" id="officer_id" class="validate" name="officer_id" value="{{ Auth::user()->id }}">
                
            <div class="modal-content">
                <h4>Loan Form</h4>                
                    <div class="input-field col s12">
                        <input type="text" id="member-id" class="validate" name="member_id">
                        <label for="member-id">Member ID</label>
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="member-nis" class="validate" placeholder="NIS" disabled>
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="member-name" class="validate" placeholder="Name" disabled>
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="member-class" class="validate" placeholder="Class" disabled>
                    </div>   
                    <div class="col s12">         
                        Status           
                        <label>
                            <input type="radio" class="loan_status" name="status" value="borrowed">
                            <span>Borrowed</span>
                        </label>
                        <label>
                            <input type="radio" class="loan_status" name="status" value="returned">
                            <span>Returned</span>
                        </label>
                    </div>                                           
            </div>
            <div class="modal-footer">
                <a href="#!" id="modal-cancel" class=" modal-action modal-close waves-effect waves-red btn-flat">Cancel</a>
                <a href="#!" id="modal-submit" class=" modal-action modal-close waves-effect waves-green btn-flat btn-disable">Submit</a>
            </div>
            </form>
        </div>
        
        <!-- Modal Structure -->
        <div id="modal-destroy" class="modal">
          <div class="modal-content">
            <h4>Destroy Data</h4>
            <p>Are you sure want delete this data ?</p>
          </div>
          <div class="modal-footer">
            <a href="#!" id="modal-cancel" class=" modal-action modal-close waves-effect waves-red btn-flat">Cancel</a>
            <a href="#!" id="modal-submit" class=" modal-action modal-close waves-effect waves-green btn-flat">Yes</a>
          </div>
        </div>

        {{--  <span class="teal-text">I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively. I am similar to what is called a panel in other frameworks.</span>  --}}
    </div>
@endsection

@push('scripts')
    <script>        
        $(document).ready(function () {   
            /**
             * Get Data Table
             */
            getData("{{ route('api.officer.book') }}", success)  
            function refresh() {
                location.reload()
            }        
            
            /**
             * Insert
             */
            $('#modal-book').modal({
                'onOpenStart': function () {
                    $('#modal-book').find('#modal-update').addClass('disabled')
                }
            })
            $('#modal-book').find('#modal-submit').click(function () {                
                postData("{{ route('api.officer.book.store') }}", $('#form-book').serialize(), function (response) { 
                    refresh()
                })
            })

            /**
             * Insert
             */
            function borrow(data) {
                var modal_borrow = $('#modal-borrow')
                    modal_borrow.modal('open')

                var form = modal_borrow.find('form')
                form.find('#member-id').change(function () {
                    getData("{{ url('api/officer/member/') }}/"+$(this).val(), 
                        function (response) { 
                            $('#member-nis').val(response.data.nis)
                            $('#member-name').val(response.data.name)
                            $('#member-class').val(response.data.class)                        
                        }
                    )
                }) 
                
                form.find('#modal-submit').click(function () {
                    var form_data = $(form).serializeArray()
                    form_data.push({name: "book_id", value: data.id})

                    postData("{{ route('api.officer.book.lending.store') }}", form_data, 
                        function (response) {
                            M.toast({
                                html: "Success",
                                outDuration: 1000,
                                classes: 'rounded',
                                completeCallback: function () { 
                                    form.trigger('reset')
                                }
                            })
                        },
                        function (error) { 
                            M.toast({
                                html: "Error: "+error.statusText,
                                outDuration: 1000,
                                classes: 'rounded',
                                completeCallback: function () { 
                                    console.log(error)
                                }
                            })
                        }
                    ) 
                })

                //location.assign("{{ route('officer.book.lending.create') }}")
            }
            
            /**
             * Update/PUT
             */
            function update(data) {
                var modal_book = $('#modal-book')
                    modal_book.modal('open')
                    modal_book.find('#modal-update').removeClass('disabled')
                    modal_book.find('#modal-submit').addClass('disabled')

                var form = modal_book.find('form')                

                $('#book-isbn').val(data.isbn)
                $('#book-title').val(data.title)
                $('#book-description').val(data.description)
                $('#book-categories').val(data.categories)
                $('#book-creator').val(data.creator)
                $('#book-publisher').val(data.publisher)
                $('#book-stock').val(data.stock)

                form.find('#modal-update').click(function () {     
                    putData("{{ url('api/officer/book/') }}/"+data.id, form.serialize(), 
                        function (response) { 
                            refresh()
                        },
                        function (error) { 
                            M.toast({
                                html: "Error: "+error.statusText,
                                outDuration: 1000,
                                classes: 'rounded',
                                completeCallback: function () { 
                                    console.log(error)
                                }
                            })
                        }
                    )           
                })
            }

            /**
             * Destroy/Delete
             */             
            function destroy(data) {
                var modal_destroy = $('#modal-destroy')
                    modal_destroy.modal('open')
                
                modal_destroy.find('#modal-submit').click(function () {
                    deleteData("{{ url('api/officer/book/') }}/"+data.id, {}, 
                        function(response) {
                            refresh()
                        },
                        function (error) { 
                            M.toast({
                                html: "Error: "+error.statusText,
                                outDuration: 1000,
                                classes: 'rounded',
                                completeCallback: function () { 
                                    console.log(error)
                                }
                            })
                        }
                    )
                })
            }

            /**
             * Callback from Get Data Table and set to DataTable
             */
            function success(response) {
                var table = $('#table-book').DataTable({
                    dom: 'Bfrtip',
                    data: response.data,
                    columns: [
                        {title: 'ID', data: 'id'},
                        {title: 'ISBN', data: 'isbn'},
                        {title: 'Title', data: 'title'},
                        {title: 'Description', data: 'description'},
                        {title: 'Categories', data: 'categories'},
                        {title: 'Creator', data: 'creator'},
                        {title: 'Publisher', data: 'publisher'},
                        {title: 'Stock', data: 'stock'},
                    ],
                    responsive: true,
                    buttons: ['pdf', 'excel']
                })

                table.on('click', 'tr', function () { 
                    var selected = table.row(this).data()

                    var modal_actions = $('#modal-actions')
                        modal_actions.modal('open')
                        modal_actions.find('#selected-id').text("ID:"+selected.id+" ISBN:"+selected.isbn)

                    modal_actions.find('ul > a').click(function () { 
                        if($(this).is('#borrow')) {                            
                            borrow(selected)
                        }
                        if($(this).is('#update')) {
                            update(selected)
                        }
                        if($(this).is('#destroy')) {
                            destroy(selected)
                        }
                    })                      
                })
            }
        })
    </script>
@endpush