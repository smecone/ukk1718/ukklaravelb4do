<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\BookLending::class, function (Faker $faker) {
    $date_loan = Carbon::instance($faker->dateTime)->toDateTimeString();
    $date_period = Carbon::parse($date_loan)->addDays(3)->toDateTimeString();

    return [
        'date_loan' => $date_loan,
        'date_period' => $date_period,
        'status' => $faker->randomElement(['borrowed', 'returned']),
        'book_id' => function () use ($faker) {
            return App\Book::find(
                $faker->biasedNumberBetween(1, App\Book::count())
            );
        },
        'member_id' => function () use ($faker) {
            return App\Member::find(
                $faker->biasedNumberBetween(1, App\Member::count())
            );
        },
        'officer_id' => function () use ($faker) {
            return App\Officer::find(
                $faker->biasedNumberBetween(1, App\Officer::count())
            );
        },
    ];
});
