<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookLendingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->id,
            'date_loan' => $this->date_loan,
            'date_period' => $this->date_period,
            'status' => $this->status,
            'book' => $this->book,
            'member' => $this->member,
            'officer' => $this->officer,
        ];
    }
}
