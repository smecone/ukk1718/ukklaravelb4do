@extends('main')

@section('title', 'Officer - Create Book Lending')
@section('sidenav', 'true')
@section('content')
    <div class="container">
        <div class="card-panel white">  
            <h4>Form Create Book Loan</h4>
            <form id="book-lending" action="" method="POST">
                {{ method_field('POST') }}
                {{ csrf_field() }}
                <input type="hidden" id="officer_id" class="validate" name="officer_id" value="{{ Auth::user()->id }}">

                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" id="book_id" class="validate" name="book_id">
                        <label for="book_id">Book ID</label>
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="book_isbn" class="validate" disabled>                
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="book_title" class="validate" disabled>                    
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="member_id" class="validate" name="member_id">
                        <label for="member_id">Member ID</label>
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="member_nis" class="validate" disabled>                
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="member_name" class="validate" disabled>                
                    </div>                
                    <div class="col s12">         
                        Status           
                        <label>
                            <input type="radio" class="loan_status" name="status" value="borrowed">
                            <span>Borrowed</span>
                        </label>
                        <label>
                            <input type="radio" class="loan_status" name="status" value="returned">
                            <span>Returned</span>
                        </label>
                    </div>                                
                </div>
                <hr/>                
                <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                    <i class="material-icons right">send</i>
                </button>
            </form>        
        </div>        
    </div>
@endsection
@push('scripts')
    <script>
        var form = $('#book-lending')

        form.find('#book_id').change(function () {

            getData("{{ url('api/officer/book') }}/"+$(this).val(), function (response) { 
                form.find('#book_isbn').val(response.data.isbn)
                form.find('#book_title').val(response.data.title)
            })
        })

        form.find('#member_id').change(function () { 
            getData("{{ url('api/officer/member') }}/"+$(this).val(), function (response) { 
                form.find('#member_nis').val(response.data.nis)
                form.find('#member_name').val(response.data.name)
            })
        })
        
        form.submit(function (event) { 
            event.preventDefault()
            console.log($(this).serialize())
            postData("{{ route('api.officer.book.lending.store') }}", $(this).serialize(), 
                function (response) { 
                    M.toast({
                        html: "Success",
                        outDuration: 1000,
                        classes: 'rounded',
                        completeCallback: function () { 
                            form.trigger('reset')
                        }
                    })
                }, 
                function (error) { 
                    M.toast({
                        html: "Error: "+error.statusText,
                        outDuration: 1000,
                        classes: 'rounded',
                        completeCallback: function () { 
                            console.log(error)
                        }
                    })
                }
            )
        })        
    </script>
@endpush