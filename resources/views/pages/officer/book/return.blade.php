@extends('main')

@section('title', 'Officer - Books Lending')
@section('sidenav', 'true')
@section('content')
    <div class="card-panel white">
        <div class="fixed-action-btn horizontal">
            <a href="{{ route('officer.book.return.create') }}" class="btn-floating btn-large waves-effect waves-light red"><i class="large material-icons">add</i></a>
        </div>

        <!-- Table -->
        <table id="table-book-return" class="display" width="100%"></table>
        
        <!-- Modal Structure -->
        <div id="modal-actions" class="modal bottom-sheet">
          <div class="modal-content">
            <h4>What you will do ? <span id="selected-id" class="badge teal-text"></span></h4>
            <ul id="actions" class="collection">
                <a href="#!" id="borrow" class="collection-item">Borrow</a>
                <a href="#!" id="update" class="collection-item">Update</a>
                <a href="#!" id="destroy" class="collection-item">Destroy</a>
            </ul>
          </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () { 
            getData("{{ route('api.officer.book.return') }}", success)

            function success(response) {
                var table = $('#table-book-return').DataTable({
                    dom: 'Bfrtip',
                    data: response,
                    columns: [
                        {title: 'ID', data: 'id'},
                        {title: 'Date Return', data: 'date_return'},
                        {title: 'Loan ID', data: 'loan_id'},
                        {title: 'Fine', data: 'fine'},
                        {title: 'Member ID', data: 'member_id'},
                        {title: 'Officer ID', data: 'officer_id'},                        
                    ],
                    buttons: ['pdf', 'excel']
                })

                table.on('click', 'tr', function () { 
                    var selected = table.row(this).data()

                    var modal_actions = $('#modal-actions')
                        modal_actions.modal('open')
                        modal_actions.find('#selected-id').text("ID:"+selected.id+" ISBN:"+selected.isbn)

                    modal_actions.find('ul > a').click(function () { 
                        if($(this).is('#borrow')) {                            
                            borrow(selected)
                        }
                        if($(this).is('#update')) {
                            update(selected)
                        }
                        if($(this).is('#destroy')) {
                            destroy(selected)
                        }
                    })                      
                })
            }
        })
    </script>
@endpush