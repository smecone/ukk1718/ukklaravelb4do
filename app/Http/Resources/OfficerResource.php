<?php

namespace App\Http\Resources;

use App\Http\Resources\BookLendingResource;
use App\Http\Resources\BookReturnResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OfficerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'name' => $this->name,
            'username' => $this->username,
            'password' => $this->password,
            'loans' => BookLendingResource::collection($this->loans),
            'returns' => BookReturnResource::collection($this->returns),
            
            // Laravel Purposes
            'api_token' => $this->api_token,
            'remember_token' => $this->remember_token,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];        
    }
}
