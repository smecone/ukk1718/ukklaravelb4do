<?php

namespace App\Http\Controllers\API;

use App\BookReturn;
use App\BookLending;
use App\Book;
use App\Http\Resources\BookReturnResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class BookReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BookReturn $bookReturn)
    {        
        return BooReturnResource::collection($bookReturn->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get Book Lending and Update "Returned"
        $bookLending = BookLending::find($request->loan_id);
        $bookLending->status = "returned";

        // Some variables
        $member = Member::find($request->member_id);
        $officer = Officer::find($request->officer_id);

        $loan_id = $bookLending->id;
        $date_period = $bookLending->date_period;
        $date_return = Carbon::now();

        // Increment Book Stock
        $book = Book::find($bookLending->book_id);
        $book->stock = ($book->stock + 1);        

        /**
         * Algorithm to count the fine
         * fine = out_period * 500/day
         */
        $fine = 0;
        if (($out_period = Carbon::parse($date_period)->diffInDays(Carbon::parse($date_return))) > 3) {
            $fine = $out_period * 500;
        }        

        $bookReturn = new BookReturn;
        $bookReturn->fine = $fine;
        $bookReturn->date_return = $date_return->toDateTimeString();
        $bookReturn->loan()->associate($loan_id);
        $bookReturn->member()->associate($member);
        $bookReturn->officer()->associate($officer);

        $book->save();
        $bookLending->save();
        $bookReturn->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BookReturn  $bookReturn
     * @return \Illuminate\Http\Response
     */
    public function show(BookReturn $bookReturn)
    {
        return new BookReturnResource($bookReturn);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BookReturn  $bookReturn
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BookReturn $bookReturn)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BookReturn  $bookReturn
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookReturn $bookReturn)
    {
        return $bookReturn->delete();
    }
}
