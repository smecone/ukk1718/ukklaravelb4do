<div class="panel-card">
    <ul class="collection with-header">
        @auth
        <li class="collection-header teal white-text">
            <h4>Welcome, {{ Auth::user()->name }}</h4>
            <span class="badge red white-text">Officer</span>
        </li>
        @endauth
        @auth('web-officer')        
        <a class="collection-item" href="{{ route('officer.home') }}"><i class="material-icons">home</i>Home</a>        
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion collection-collapsible">
                <li><a class="collapsible-header collection-item"><i class="material-icons">filter_drama</i>Books</a>
                <div class="collapsible-body">
                    <ul class="collection">
                        <a href="{{ route('officer.book') }}" class="collection-item">Books</a>
                        <a href="{{ route('officer.book.lending') }}" class="collection-item">Books Lending</a>
                        <a href="{{ route('officer.book.lending.create') }}" class="collection-item">Form Books Lending</a>
                        <a href="{{ route('officer.book.return') }}" class="collection-item">Books Return</a>
                        <a href="{{ route('officer.book.return.create') }}" class="collection-item">Form Books Return</a>
                    </ul>                        
                </div>
                </li>
            </ul>
        </li>
        <a href="{{ route('officer.member') }}" class="collection-item">Member List</a>
        <a href="{{ route('officer.list') }}" class="collection-item">Officer List</a>            
        @endauth
    </ul>
</div>