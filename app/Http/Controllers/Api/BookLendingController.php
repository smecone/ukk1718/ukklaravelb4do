<?php

namespace App\Http\Controllers\API;

use App\BookLending;
use App\Book;
use App\Member;
use App\Officer;
use App\Http\Resources\BookLendingResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class BookLendingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BookLending $bookLending)
    {        
        return BookLendingResource::collection($bookLending->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date_loan = Carbon::now()->toDateTimeString();
        $date_period = Carbon::parse($date_loan)->addDays(3)->toDateTimeString();

        $bookLending = new BookLending;
        $book = Book::find($request->book_id);
        $member = Member::find($request->member_id);
        $officer = Officer::find($request->officer_id);
        
        $bookLending->date_loan = $date_loan;
        $bookLending->date_period = $date_period;
        $bookLending->status = $request->status;
        $bookLending->book()->associate($book);
        $bookLending->member()->associate($member);
        $bookLending->officer()->associate($officer);

        $book->stock = ($book->stock - 1);
        $book->save();

        return response()->json($bookLending->save());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BookLending  $bookLending
     * @return \Illuminate\Http\Response
     */
    public function show(BookLending $bookLending)
    {
        return new BookLendingResource($bookLending);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BookLending  $bookLending
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BookLending $bookLending)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BookLending  $bookLending
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookLending $bookLending)
    {
        return $bookLending->delete();
    }
}
