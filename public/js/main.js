function ajaxPost(url, method, data, success, fail) {
    $.ajax({
        type: method,
        url: url,
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Authorization': window.Laravel.api_token
        }
    }).done(function (response) { 
        console.log(response)        
        if (typeof success === 'function') {
            success(response)                
        }
    }).fail(function (error) {
        console.log(error)
        if (typeof fail === 'function') {
            fail(error)
        }
    })
}

function getData(url, success, fail) {
    $.ajax({
        type: "GET",
        url: url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Authorization': window.Laravel.api_token
        }
    }).done(function (response) { 
        console.log(response)        
        if (typeof success === 'function') {
            success(response)                
        }
    }).fail(function (error) {
        console.log(error)
        if (typeof fail === 'function') {
            fail(error)
        }
    })
}

function postData(url, data, success, fail) {
    ajaxPost(url, "POST", data, success, fail)
}

function putData(url, data, success, fail) {
    ajaxPost(url, "PUT", data, success, fail)
}

function deleteData(url, data, success, fail) {
    ajaxPost(url, "DELETE", data, success, fail)
}