<?php

use Faker\Factory as Faker;

$factory->define(App\Officer::class, function () {
    $faker = Faker::create(config('app.locale'));

    return [
        'name' => $faker->name,
        'username' => $faker->unique()->userName,
        'password' => $faker->password,
        'api_token' => str_random(80),
    ];
});
