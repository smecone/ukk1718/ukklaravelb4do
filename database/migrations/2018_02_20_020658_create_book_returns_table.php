<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_returns', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('fine');
            $table->dateTime('date_return');
            
            $table->integer('loan_id')->unsigned();            
            $table->integer('member_id')->unsigned();
            $table->integer('officer_id')->unsigned();            
            
            // Relations
            $table->foreign('loan_id')->references('id')->on('book_lendings');
            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('officer_id')->references('id')->on('officers');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_returns');
    }
}
