<?php

namespace App\Http\Controllers\API;

use App\Book;
use App\Http\Resources\BookResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Book $book)
    {
        return BookResource::collection($book->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book = new Book;

        $book->isbn = $request->isbn;
        $book->title = $request->title;
        $book->description = $request->description;
        $book->categories = $request->categories;
        $book->creator = $request->creator;
        $book->publisher = $request->publisher;
        $book->stock = $request->stock;

        return response()->json($book->save());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return new BookResource($book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $book->isbn = $request->isbn;
        $book->title = $request->title;
        $book->description = $request->description;
        $book->categories = $request->categories;
        $book->creator = $request->creator;
        $book->publisher = $request->publisher;
        $book->stock = $request->stock;

        return response()->json($book->save());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        return response()->json($book->delete());
    }
}
