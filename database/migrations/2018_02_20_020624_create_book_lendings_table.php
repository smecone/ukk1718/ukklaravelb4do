<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookLendingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_lendings', function (Blueprint $table) {
            $table->increments('id');

            $table->dateTime('date_loan');
            $table->dateTime('date_period');
            $table->enum('status', ['borrowed', 'returned']);
            
            $table->integer('book_id')->unsigned();
            $table->integer('member_id')->unsigned();
            $table->integer('officer_id')->unsigned();            

            // Relations
            $table->foreign('book_id')->references('id')->on('books');
            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('officer_id')->references('id')->on('officers');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_lendings');
    }
}
