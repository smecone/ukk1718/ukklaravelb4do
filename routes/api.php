<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

/**
 * Officer API Route
 */
Route::middleware('auth:api-officer')->group(function () {        
    Route::prefix('/officer')->group(function () {
        Route::prefix('/book')->group(function () {
            Route::apiResource('/lending', 'BookLendingController', [
                'names' => [
                    'index' => 'api.officer.book.lending',
                    'store' => 'api.officer.book.lending.store',
                    'show' => 'api.officer.book.lending.show',
                    'update' => 'api.officer.book.lending.update',
                    'destroy' => 'api.officer.book.lending.destroy',
                ],
                'parameters' => [
                    'lending' => 'bookLending'
                ]
            ]);
    
            Route::apiResource('/return', 'BookReturnController', [
                'names' => [
                    'index' => 'api.officer.book.return',
                    'store' => 'api.officer.book.return.store',
                    'show' => 'api.officer.book.return.show',
                    'update' => 'api.officer.book.return.update',
                    'destroy' => 'api.officer.book.return.destroy',
                ],
                'parameters' => [
                    'return' => 'bookReturn'
                ]
            ]);
        });      
        Route::apiResource('/book', 'BookController', [
            'names' => [
                'index' => 'api.officer.book',
                'store' => 'api.officer.book.store',
                'show' => 'api.officer.book.show',
                'update' => 'api.officer.book.update',
                'destroy' => 'api.officer.book.destroy',
            ]
        ]);  

        Route::apiResource('/member', 'MemberController', [
            'except' => [
                'store'
            ],
            'names' => [
                'index' => 'api.officer.member',
                'store' => 'api.officer.member.store',
                'show' => 'api.officer.member.show',
                'update' => 'api.officer.member.update',
                'destroy' => 'api.officer.member.destroy',
            ]
        ]);
    });
    Route::apiResource('/officer', 'OfficerController', [
        'except' => [
            'store'
        ],
        'names' => [
            'index' => 'api.officer',
            'store' => 'api.officer.store',
            'show' => 'api.officer.show',
            'update' => 'api.officer.update',
            'destroy' => 'api.officer.destroy',
        ]
    ]);
});