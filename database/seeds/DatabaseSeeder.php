<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call([
            OfficerTableSeeder::class,
            MemberTableSeeder::class,
        ]);

        factory(App\Officer::class, 20)->create();
        factory(App\Member::class, 20)->create();
        factory(App\Book::class, 40)->create();
        factory(App\BookLending::class, 30)->create();
        factory(App\BookReturn::class, 20)->create();
    }
}
