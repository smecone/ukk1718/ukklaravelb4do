<!DOCTYPE html>
<html lang="en">
<head>    
    {{-- Stylesheet --}}
    <link rel="stylesheet" href="{{ url('/') }}/fonts/material/icons.css">    
    <link rel="stylesheet" href="{{ url('/') }}/css/materialize.css" media="screen, projection">    
    <link rel="stylesheet" href="{{ url('/') }}/css/main.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/datatables.css">    
    @yield('head')
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }} - @yield('title')</title>
</head>
<body>
    {{-- Navbar --}}
    <nav>
        <div class="nav-wrapper light-green">
            <div class="container">
                <a href="#" class="brand-logo"><i class="material-icons large">face</i></a>
                {{-- <a href="#" data-activates="mobile-menu" class="button-collapse"><i class="material-icons">menu</i></a> --}}
                <ul class="right hide-on-med-and-down">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li><a href="{{ route('officer.home') }}">Officer</a></li>
                <li><a href="{{ route('member.home') }}">Member</a></li>
                <li><a href="{{ route('officer.logout') }}">Logout</a></li>
                </ul>
                {{--<ul class="side-nav" id="mobile-menu">
                <li><a href="#">item1</a></li>
                <li><a href="#">item2</a></li>
                <li><a href="#">item3</a></li>
                <li><a href="#">item4</a></li>
                </ul>--}}
            </div>
        </div>
    </nav>

    {{-- The Main Content --}}
    @hasSection('sidenav')
        <div class="row">
            <div class="col s3">
                {{-- Left Content --}}
                @include('sidenav')
            </div>
            <div class="col s9">            
                {{-- Content --}}
                @yield('content')
            </div>
        </div>
    @else
        {{-- Content --}}
        @yield('content')
    @endif    
{{-- Javascript --}}
<script>
    window.Laravel = {
        baseUrl: '{{ url('/') }}',
        csrf_token: '{{ csrf_token() }}',
        @auth
        api_token: 'Bearer {{ Auth::user()->api_token }}'
        @endauth
    }
</script>
<script src="{{ url('/') }}/js/lib/lodash.js"></script>
<script src="{{ url('/') }}/js/lib/jquery-3.3.1.js"></script>
<script src="{{ url('/') }}/js/lib/materialize.js"></script>
<script src="{{ url('/') }}/js/lib/datatables.js"></script>
<script src="{{ url('/') }}/js/main.js"></script>
<script>
    $(document).ready(function () {
        $('.side-nav').sidenav()
        $('.modal').modal()
        $('.collapsible').collapsible();
    })
</script>
@stack('scripts')
</body>
</html>