<?php

namespace App\Http\Resources;

use App\Http\Resources\BookLendingResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'isbn' => $this->isbn,
            'title' => $this->title,
            'description' => $this->description,
            'categories' => $this->categories,
            'creator' => $this->creator,
            'publisher' => $this->publisher,
            'stock' => $this->stock,
            'loans' => BookLendingResource::collection($this->loans),
        ];
    }
}
